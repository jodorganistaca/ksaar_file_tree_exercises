import React from 'react'
import { Box, Typography } from '@mui/material'
import { FileRow } from './components/FileRow'
import { useWorkspaceContext } from '../Workspace/Workspace.context'
import { TreeView } from '@mui/x-tree-view/TreeView';
import { TreeItem } from '@mui/x-tree-view/TreeItem';

interface RenderTree {
  id: string;
  name: string;
  children?: readonly RenderTree[];
}

export const FilePane = () => {
  const { files } = useWorkspaceContext();

  const renderTree = (nodes: RenderTree) => (
    <TreeItem key={nodes.id} nodeId={nodes.id} label={nodes.name}>
      {Array.isArray(nodes.children)
        ? nodes.children.map((node) => renderTree(node))
        : null}
    </TreeItem>
  );

  function updateData(elem, head, tail, content) {
    const [value, ...t] = tail;
    let elemIndx = elem.findIndex(e => e.name === head);
    if (elemIndx > -1) {
      if (value !== undefined) {
        updateData(elem[elemIndx].children, value, t, content);
      }
    } else {
      if (value === undefined) {
        elem.push({ id: head, name: head, content });
      } else {
        console.log(t);
        if (t.length == 0) {
          elem.push({ id: head, name: head, children: [{ id: value, name: value, content }] });
        } else {
          elem.push({ id: head, name: head, children: [{ id: value, name: value, children: [] }] });
        }

      }
    }
    return elem;
  }

  const transformFilesToData = (files) => {
    let paths = [];
    let keys = {};
    files.forEach(function (value, i) {
      let path = value.path.split('/');
      const [head, ...tail] = path;
      path.forEach(function (v, ind) {
        if (ind in keys === false) {
          keys[ind] = new Set();
        }
        if (ind + 1 < path.length) {
          keys[ind].add(JSON.stringify({ value: v, child: path[ind + 1] }));
        } else {
          keys[ind].add(JSON.stringify({ value: v, child: "" }));
        }
        updateData(paths, head, tail, value.contents);
      });
    });
    console.log("paths ", paths);
    return paths[0];
  }

  return (
    <Box>
      <Box p={1}>
        <Typography variant="h6">Files</Typography>
      </Box>
      <Box>
        {
          files.map((file) => <FileRow key={file.path} file={file} />)
        }
        <TreeView
          aria-label="file system navigator"
          sx={{ height: 240, flexGrow: 1, maxWidth: 400, overflowY: "auto" }}
        >
          {
            renderTree(transformFilesToData(files))
            // files.map((file) => <FileRow key={file.path} file={file} />)
          }
        </TreeView>
      </Box>
    </Box>
  )
}
